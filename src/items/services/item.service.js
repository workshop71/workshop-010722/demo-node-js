/**
 * In-Memory Store
 */

const items = {
  1: {
    id: 1,
    name: "Burger",
    price: 5.99,
    description: "Tasty",
    image: "https://cdn.auth0.com/blog/whatabyte/burger-sm.png"
  },
  2: {
    id: 2,
    name: "Pizza",
    price: 2.99,
    description: "Cheesy",
    image: "https://cdn.auth0.com/blog/whatabyte/pizza-sm.png"
  },
  3: {
    id: 3,
    name: "Tea",
    price: 1.99,
    description: "Informative",
    image: "https://cdn.auth0.com/blog/whatabyte/tea-sm.png"
  }
};

for (var i = 4; i < 10000; i++) {
  items[i] = {
    id: i,
    name: "Tea",
    price: 1.99,
    description: "Informative",
    image: "https://cdn.auth0.com/blog/whatabyte/tea-sm.png"
  };
}

/**
 * Service Methods
 */

exports.findAll = async () => {
  return items;
};

exports.find = async (id) => {
  const record = items[id];

  if (record) {
    return record;
  }

  throw new Error("No record found");
};

exports.create = async (newItem) => {
  const id = new Date().valueOf();
  items[id] = {
    ...newItem,
    id
  };
};

exports.update = async (updatedItem) => {
  if (items[updatedItem.id]) {
    items[updatedItem.id] = updatedItem;
    return;
  }

  throw new Error("No record found to update");
};

exports.remove = async (id) => {
  const record = items[id];

  if (record) {
    delete items[id];
    return;
  }

  throw new Error("No record found to delete");
};
