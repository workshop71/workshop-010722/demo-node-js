import http from 'k6/http';
import { check, sleep } from 'k6';

export default function () {
  let res = http.post(
    `http://localhost:8000/items`,
    JSON.stringify({
      // prev: '92b56ac440729da7',
      // columnName: (Math.random() * 200000).toString(),
      price: 1.99,
      description: "Informative",
      image: "https://cdn.auth0.com/blog/whatabyte/tea-sm.png"
    }),
    { headers: { 'Content-Type': 'application/json' } }
  );

  check(res, { 'status was 201': (r) => r.status == 201 });
}