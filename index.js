/**
 * Required External Modules
 */

 const dotenv = require("dotenv");
 const express =  require("express");
 const cors = require("cors");
 // import helmet from "helmet";
 const itemsRouter = require("./src/items/item.router");
 
 dotenv.config();
 
 /**
  * App Variables
  */
 
 if (!process.env.PORT) {
   process.exit(1);
 }
 
 const PORT = parseInt(process.env.PORT, 10);
 
 const app = express();

 /**
 *  App Configuration
 */

// app.use(helmet());
app.use(cors());
app.use(express.json());
app.use("/items", itemsRouter);

/**
 * Server Activation
 */

 const server = app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});